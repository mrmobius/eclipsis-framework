#pragma once
enum class FrameBufferType
{
	Position,
	Normal,
	Albedo,
	PBR,
	Glow,
	Num_Buffers
};
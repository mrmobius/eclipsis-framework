#pragma once
enum class BufferType
{
	Vertex,
	Normal,
	Tangent,
	BiTangent,
	TextureCoordinate,
	Index,
	Num_Buffers
};
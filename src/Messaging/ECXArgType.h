#pragma once

enum class ECXArgType
{
	CHAR,
	BOOL,
	INT,
	U_INT,
	FLOAT,
	DOUBLE,
	STRING,
	VECTOR,
};

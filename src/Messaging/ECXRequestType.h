#pragma once

enum class ECXRequestType
{
	FrustumCheck,
	RayCheck,
	EntitySearch
};
#pragma once

enum class ECXResponseType
{
	Success,
	Fail,
	Unsupported
};
#pragma once
enum class EC_SystemType
{
	Spatial,
	Transform,
	Camera,
	Collision,
	Physics,
	AI,
	Animation,
	Particle,
	Network,
	Scripting,
	Num_Systems
};
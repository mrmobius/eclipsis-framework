#pragma once
#include <string>
#include <vector>
struct GameSettings
{
	std::string window_settings_file;
	std::vector<std::string> GameModes;
};
#pragma once
enum class MouseButton
{
	LMB,
	RMB,
	Middle,
	MB4,
	MB5,
	Motion
};
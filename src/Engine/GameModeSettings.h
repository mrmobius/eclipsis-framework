#pragma once
#include <string>
#include <vector>

struct GameModeSettings
{
	std::string engine_settings;
	std::string controls;
	std::string game_world_data;
};